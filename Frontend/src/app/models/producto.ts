
export interface ProdcutoModel{
    idProductos: number,
    Nombre: string,
    Tipo: string,
    Precio: number,
    Descrip: string,
    Foto: string
}